import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReverseLargest{
	
	public static void main(String[] args){
		try {
		BufferedReader buffer = new BufferedReader(new FileReader(args[0]));
		String fileLine = buffer.readLine();
		StringBuffer largest = new StringBuffer();
		
		while (fileLine != null) {
			if ( fileLine.length() > largest.length()){
				largest.delete(0, largest.length());
				largest.append(fileLine);
			}
			fileLine = buffer.readLine();
		}
		buffer.close();
		
		System.out.println(largest);
		System.out.println(new StringBuffer(largest).reverse());
		}
		catch (IOException e) {
			System.out.println("Error: " + e + " while reading file " + args[0]);
		}
	}
	
}